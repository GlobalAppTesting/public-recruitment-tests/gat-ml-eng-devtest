# ML engineer test
We hope this task will give you a possibility to present your knowledge
in the ML area as well as general coding abilities. But, what is even more
important to us is: you have a lot of fun when solving our task!

## The task
Pick one of the environments from OpenAI gym:

CarRacing-v0: https://gym.openai.com/envs/CarRacing-v0/

or

FrozenLake8x8-v0 (or v1, depending on the Gym version you are using): https://gym.openai.com/envs/FrozenLake8x8-v0/

Could you provide a working solution to this problem? Of course, do not
try to use provided solutions for the chosen task.

## What is important to us? 
We want to see your ability to create a working model for the reinforcement
learning task. The very first thing is to create a model which can
effectively play and complete the game.

We will also take a look on your coding skills. It is important to us
to see a clear and self-describing solution. Show us you can write a PEP-8 
complied code. Don't forget about a few tests!

Make sure your solution contains README file which allows us to quickly
setup and run your code. The dependencies should be clearly stated and easy
to install. A simple Dockerfile is never too much.

If there are any artifacts (learned models, graphs, logs) related to the task,
feel free to commit them to the repo.
